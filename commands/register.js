const config = require('../config.json');
const talkedRecently = new Set();

var mysql = require('mysql');
var authconnection = mysql.createConnection({
    host: config.dbhost,
    port: 3306,
    user: config.dbuser,
    password: config.dbpw,
    database: config.auth
});

exports.run = (bot, msg, args) => {
if (msg.channel.type != 'dm') {
        msg.delete()
        msg.channel.send("Please register an account through private message.")
    } else {
        if (talkedRecently.has(msg.author.id)) {
            msg.reply("You may only register a maximum of one account per day.")
        } else {
            let accName = args[0];
            let passwd = args[1];
            authconnection.query('CALL create_account (?,?)', [accName, passwd], function (error, results, fields) {
                if (args[0] == null || args[1] == null) {
                    msg.reply("Please properly fill in each field: `.register username password`");
                } else if (error) {
                    msg.reply("There was an error.")
                        console.error(error);
                        return;
                    } else {
                        if (results != "") {
                            msg.reply("Your account has been created: " + accName);
                            msg.reply("Remember to set your realmlist to `set realmlist logon.lostislesf2p.com`")
                            talkedRecently.add(msg.author.id);
                            setTimeout(() => {
                                talkedRecently.delete(msg.author.id);
                            }, 86400000);
                        } else {
                            console.log('.register error')
                            msg.reply("Something went wrong.")
                        }
                    }
                })
        }
    }
}