exports.run = (bot, msg, args) => {
    if (msg.member.hasPermission('MANAGE_MESSAGES')) {
        let numToDelete = args[0];
        if (numToDelete < 2) {
            msg.author.sendMessage("You cannot purge just one message.");
        } else if (numToDelete > 100) {
            msg.author.sendMessage("You cannot purge more than 100 messages at a time.");
        }
        else {
            msg.channel.bulkDelete(numToDelete)
                .then(messages => msg.reply("``` Deleted " + numToDelete + " messages. ```"))
                .catch(err => console.error(err))
        }
    }
}