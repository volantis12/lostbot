const config = require('../config.json');

var mysql = require('mysql');
var authconnection = mysql.createConnection({
    host: config.dbhost,
    port: 3306,
    user: config.dbuser,
    password: config.dbpw,
    database: config.auth
});

exports.run = (bot, msg, args) => {
    if (msg.channel.type != 'dm') {
        msg.delete()
        msg.channel.send("Please change your password through private message.")
    } else {
        let accName = args[0];
        let passwd = args[1];
        let newpass = args[2];
        authconnection.query('CALL change_pass (?,?,?)', [accName, passwd, newpass], function (error, results, fields) {
            if (error) {
                msg.reply("There was an error.")
                console.error(error);
                return;
            } else {
                if (args[0] == null || args[1] == null || args[2] == null) {
                    msg.reply("Please properly fill in each field: `.changepass username password newpassword`");
                } else if (results != "") {
                    msg.reply("If username/password matched, then your password has been changed.");
                } else {
                    console.log('.changepass error.')
                    msg.reply("Something went wrong.")
                }
            }
        })
    }
}