const config = require('../config.json');
const Discord = require("discord.js");

var net = require('net');

function sleepTime(ms) {
    var date = new Date();
    var curDate = null;

    do { curDate = new Date(); }
    while (curDate - date < ms);
}

exports.run = (bot, msg, args) => {
    let charNameRaw = args[0];      // Character Name
    let itemID = args[1];           // Donation reward ID

    if (msg.channel.name === "donate-rewards" && msg.author.id === "457918998303211530") {
        if (args[0] == null || args[1] == null) {
            msg.reply("Please properly fill in each field: `.reward charname itemid`");
        } else if (itemID > 0 && charNameRaw != "") {
            const charName = charNameRaw.charAt(0).toUpperCase() + charNameRaw.slice(1);    // Sanitized character name
            var socket = net.createConnection(3443, '127.0.0.1', function () {      // Open TelNet Connection - DO NOT TOUCH ANYTHING HERE PLEASE GOD
                console.log("Authenticating...")
                sleepTime(2000);    // REQUIRED
                socket.write(config.rauser + "\r\n");
                socket.write(config.rapass + "\r\n");
                socket.write("send items " + charName + " \"The Lost Isles Shop\" \"Thank you for your support! If you need help, feel free to ask a staff member.\"" + itemID + " \r\n");
                sleepTime(2000);    // REQUIRED
            });

            socket.on("data", function (data) {
                console.log(data.toString());
                socket.end();
            })

            socket.on("error", function (err) {
                console.log("Error");
                console.log(err);
            })

            socket.on("end", function (err) {
                console.log("Connection closed.");
            })

            msg.channel.send(charName + " has been rewarded with item ID: " + itemID);
        } else {
            console.log('.reward error.')
            msg.reply("Something went wrong.")
        }
    } else if (msg.channel.name === "donate-rewards" && msg.author.id != "108176121463332864") {
        msg.channel.send("You don't have permission to do that.")
    } else if (msg.channel.name != "donate-rewards") {
        return;
    }
}