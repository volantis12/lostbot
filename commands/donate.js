const config = require('../config.json');
const Discord = require("discord.js");


exports.run = (bot, msg, args) => {
    if (msg.channel.name === "bot-commands") {
        msg.channel.send({
            "embed": {
                "title": "Our donations & rewards are automated through Selly & LostBot!",
                "description": "If you have any questions, or want something that isn't offered in the shop, contact an administrator on Discord.",
                "url": "https://selly.gg/u/TheLostIsles/groups",
                "color": 623843,
                "timestamp": "2018-06-17T01:56:46.038Z",
                "footer": {
                    "icon_url": "https://cdn.discordapp.com/embed/avatars/0.png",
                    "text": "Thanks for your support!"
                },
                "thumbnail": {
                    "url": "https://cdn.discordapp.com/embed/avatars/0.png"
                },
                "author": {
                    "name": "The Lost Isles Shop",
                    "url": "https://selly.gg/@TheLostIsles",
                    "icon_url": "https://cdn.discordapp.com/embed/avatars/0.png"
                }
            }
        });
    } else {
        return;
    }
}