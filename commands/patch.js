const config = require('../config.json');


exports.run = (bot, msg, args) => {
    msg.channel.send("You can download our patch at https://dbr.ee/5dJG (Direct Link)")
    msg.channel.send("Please place this in 'WoW/Data/Cache/' and make sure to overwrite the existing file.")
    msg.channel.send("If you are having any issues, please contact Volantis.")
}