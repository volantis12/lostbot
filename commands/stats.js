const config = require('../config.json');
const Discord = require("discord.js");

var mysql = require('mysql');
var charconnection = mysql.createConnection({
    host: config.dbhost,
    port: 3306,
    user: config.dbuser,
    password: config.dbpw,
    database: config.characters
});

function precise(x) {
    return Number.parseFloat(x).toPrecision(4);
}

exports.run = (bot, msg, args) => {
    let charNameRaw = args[0];
    const charName = charNameRaw.charAt(0).toUpperCase() + charNameRaw.slice(1);
    var sql = 'SELECT totalKills, leveltime, name, race, class FROM characters WHERE name = ' + charconnection.escape(charName);
    charconnection.query(sql, function (error, results, fields) {
        if (error) {
            msg.channel.send("There was an error.")
            console.error(error);
            return;
        } else {
            if (results != "") {
                var raceID = results[0].race;
                var classID = results[0].class;

                switch (raceID) {
                    default:
                        var race = "unknown";
                        break;
                    case 1:
                        var race = 'Human';
                        break;
                    case 2:
                        var race = 'Orc';
                        break;
                    case 3:
                        var race = 'Dwarf';
                        break;
                    case 4:
                        var race = 'Night Elf';
                        break;
                    case 5:
                        var race = 'Undead';
                        break;
                    case 6:
                        var race = 'Tauren';
                        break;
                    case 7:
                        var race = 'Gnome';
                        break;
                    case 8:
                        var race = 'Troll';
                        break;
                    case 9:
                        var race = 'Goblin';
                        break;
                    case 10:
                        var race = 'Blood Elf';
                        break;
                    case 11:
                        var race = 'Draenei';
                        break;
                    case 22:
                        var race = 'Worgen';
                        break;
                }

                switch (classID) {
                    default:
                        var classname = "Unknown";
                        break;
                    case 1:
                        var classname = "Warrior";
                        break;
                    case 2:
                        var classname = "Paladin";
                        break;
                    case 3:
                        var classname = "Hunter";
                        break;
                    case 4:
                        var classname = "Rogue";
                        break;
                    case 5:
                        var classname = "Priest";
                        break;
                    case 6:
                        var classname = "Death Knight";
                        break;
                    case 7:
                        var classname = "Shaman";
                        break;
                    case 8:
                        var classname = "Mage";
                        break;
                    case 9:
                        var classname = "Warlock";
                        break;
                    case 11:
                        var classname = "Druid";
                        break;
                }

                var timePlayed = ((results[0].leveltime) / 3600);
                const embed = new Discord.RichEmbed()
                  .setTitle(charName)
                  .setColor(0x00AE86)
                  .setDescription(race + " - " + classname)
                  .setFooter("Stats taken")
                  .setTimestamp()
                  .addField("Time Played", precise(timePlayed) + " Hours")
                  .addField("Honorable Kills", results[0].totalKills);
                msg.channel.send({ embed });
            }
            else {
                console.log('.stats could not find name')
                msg.channel.send("Name not found or invalid.")
            }
        }
    });
}