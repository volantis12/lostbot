'use strict';
const config = require('./config.json');
const Discord = require("discord.js");
const bot = new Discord.Client();
const fs = require('fs');


bot.on('guildMemberAdd', member => {
    member.send("Welcome to The Lost Isles!");
    member.send("To get started, reply to me with '.register username password' - This is how you create your in-game account.");
    member.send("If you have any questions, head over to the #bot-help channel, or PM a staff member.");
});
bot.on('ready', () => {
    console.log('LostBot Online.');
    bot.user.setGame('LostIslesf2p.com');
});
bot.on('message', msg => {
    if (!msg.content.startsWith(config.prefix)) return;
    if (msg.channel.name != "bot-commands" && msg.channel.type != 'dm' && msg.channel.name != "donate-rewards") return;
    let command = msg.content.split(" ")[0];
    command = command.slice(config.prefix.length);
    let args = msg.content.split(" ").slice(1);
    try {
        let commandFile = require(`./commands/${command}.js`);
        commandFile.run(bot, msg, args);
    } catch (err) {
        console.error(err);
    }
});
//Debugging
bot.on('error', (e) => console.error(e));
bot.on('warn', (e) => console.warn(e));
bot.on('debug', (e) => console.info(e));
bot.login(config.token);